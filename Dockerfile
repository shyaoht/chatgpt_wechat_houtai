FROM openjdk:8-jdk
FROM maven:3.6.3-jdk-8
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo "Asia/Shanghai" > /etc/timezone
WORKDIR /app
COPY . /app
RUN mvn clean
RUN  mvn clean package -Dmaven.test.skip=true
RUN cp $(find /app -type f -name "ruoyi-admin.jar") /app/

RUN cp ruoyi-admin/src/main/resources/application.yml /app && \
    cp ruoyi-admin/src/main/resources/application-druid.yml /app
ENV SERVER_PORT=8081
ENV REDIS_HOST=127.0.0.1
ENV REDIS_PORT=6379
ENV REDIS_PASSWORD=
ENV MYSQL_HOST=127.0.0.1
ENV MYSQL_NAME=yourmysqlname
ENV MYSQL_USERNAME=yourmysqlusername
ENV MYSQL_PASSWORD=yourmysqlpassword
ENV MYSQL_PORT=3306
CMD ["java", "-jar", "ruoyi-admin.jar", "--spring.config.name=application,application-druid"]
